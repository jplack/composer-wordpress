# Docker Composer WordPress 
This is a boilerplate project for a single WordPress installation using Docker and Composer.

## Required Tools

- [Docker](https://www.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)
- [Composer](https://getcomposer.org/)
- [WP-CLI](https://wp-cli.org/)

## Recommended
- [wp-cli-dotenv-command](wp-cli-dotenv-command)

## Getting Started

1. Clone the Repo
2. `composer install --prefer-dist`
3. set the desired environment variables in `.env`
4. `wp dotenv salts generate` 
5. `docker-compose up -d`
6. WordPress will be running at the APP_PORT specified in `.env` (8000 by default)

## Adding themes and plugins

You can install themes and plugins using composer and WPackagist   
**Example:**   
`composer require "wpackagist-plugin/jetpack":"^7.4.0"`

You can also install your own custom themes and plugins from Git using Composer, [even for private repositories](https://getcomposer.org/doc/05-repositories.md#using-private-repositories).   
composer.json:     
```
{
    "require": {
        "vendor/my-private-repo": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:vendor/my-private-repo.git"
        }
    ]
}
```

It is recommended to create seperate repos for your custom themes and plugins and require them with Composer as opposed to editing the files directly in your main WordPress project's repo. This way everything is nicely versioned and you can easily deploy a mix of different versions for theme and plugins in different environments.


